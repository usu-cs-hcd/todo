// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};
Alloy.Globals.SituationConstants = {
    Types: Object.freeze({
        LOCATION: "Location",
        TIME:     "Time",
        MANUAL:   "Manual"
    })
};

Alloy.Globals.Map = require('ti.map');

var db = Ti.Database.open('_alloy_');
//db.execute('DROP TABLE IF EXISTS situation;');
//db.execute('DROP TABLE IF EXISTS skill;');
//db.execute('DROP TABLE IF EXISTS skillSituation;');
db.execute('DROP TABLE IF EXISTS responseLog;');
db.close ();
