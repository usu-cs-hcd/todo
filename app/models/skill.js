exports.definition = {
    config: {
        columns: {
/*
 "id": "int",
 "name": "text",
 "data": "text"
 */
 "skillId" : "INTEGER PRIMARY KEY AUTOINCREMENT",
 "name": "text",
 "question": "text",
 "desiredAnswer": "text",
 "responseType": "text",
 "frequencyEvery": "text",
 "frequencyUnit": "text",
 "vibration": "text",
 "soundName": "text" ,
 "lastAlarm": "text"

        },
        adapter: {
            type: "sql",
            collection_name: "skill",
            idAttribute: 'skillId'
        },
        idAttribute: 'skillId'
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
		});

		return Collection;
	}
};