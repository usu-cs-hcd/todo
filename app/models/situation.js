exports.definition = {
	config: {
		columns: {
		    "situationId": "INTEGER PRIMARY KEY AUTOINCREMENT",
		    "name": "TEXT",
			"image": "TEXT",
		    "data": "TEXT"
		},
		adapter: {
			type: "sql",
			collection_name: "situation"
		},
		idAttribute: 'situationId'
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
		});

		return Collection;
	}
};