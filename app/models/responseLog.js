exports.definition = {
	config: {
		columns: {
			"time": "TEXT",
			"skillName": "TEXT",
			"response": "TEXT",
			"mechanism": "TEXT"
		},
		adapter: {
			type: "sql",
			collection_name: "responseLog"
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
		});

		return Collection;
	}
};