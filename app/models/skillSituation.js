exports.definition = {
	config: {
		columns: {
		    "situationId": "INTEGER",
		    "skillId":"INTEGER"
		},
		adapter: {
			type: "sql",
			collection_name: "skillSituation"
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
		});

		return Collection;
	}
};