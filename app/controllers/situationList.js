/**
 * Created by kaychoro on 4/8/15.
 */
var situationCollection = Alloy.Collections.situation;
var skillCollection = Alloy.Collections.skill;
var skillSituationMap = Alloy.Collections.skillSituation;
situationCollection.fetch();
skillCollection.fetch();
skillSituationMap.fetch();

var subLineViewDefinition = {
    width: Ti.UI.FILL,
    right: 0,
    height: .5,
    backgroundColor: "gray"
};

var setupLoader = false;

var expandAnimation = Titanium.UI.createAnimation({
    height: "300",
    duration: 125
});
var contractAnimation = Titanium.UI.createAnimation({
    height: "41",
    duration: 125
});
var flippedMatrix = Ti.UI.create2DMatrix({
    rotate: 180
});
var flipAnimation = Ti.UI.createAnimation({
    transform: flippedMatrix,
    duration: 125
});
var unflippedMatrix = Ti.UI.create2DMatrix({
    rotate: 0
});
var unflipAnimation = Ti.UI.createAnimation({
    transform: unflippedMatrix,
    duration: 125
});
var entryExpanded = false;
var containerView = false;

function editSituation(event) {
    if (entryExpanded) {
        doButtonClick({source: entryExpanded});
        return;
    }
    var selectedSituation = event.source;
    //if the click event comes on a sub element, we need to make sure we get to the parent object first
    while (!selectedSituation.situationId) {
        selectedSituation = selectedSituation.parent;
    }
    selectedSituation.backgroundColor = "black";
    setTimeout(function(){selectedSituation.backgroundColor = "white";
        var situationModel = situationCollection.where({situationId: selectedSituation.situationId})[0];
        var args = {
            situationId: selectedSituation.situationId,
            data: situationModel.get('data')
        };

        openView("situationDetails", args);
    }, 5);
}

function addSituation() {
    openView("situationDetails", {});
}

function openView(controllerName, args) {
    showLoading();
    var controller = Alloy.createController(controllerName, args);
    var view = controller.getView();
    view.addEventListener("close", onSituationDetailClose);
    if (OS_IOS) {
        $.navGroupWin.openWindow(view);
    }
    if (OS_ANDROID) {
        view.open();
    }
    setTimeout(hideLoading, 50);
}

function doButtonClick(event) {
    var button = event.source;
    //close prior one
    if (entryExpanded && entryExpanded != null) {
        entryExpanded.animate(unflipAnimation);
        var priorExpandedSituationRow = entryExpanded.parent;
        priorExpandedSituationRow.animate(contractAnimation);
        //$.situationScroller.height = Ti.UI.SIZE;
        var priorSituationSkillMappingView = containerView;
        setTimeout(function(){
            priorExpandedSituationRow.remove(priorSituationSkillMappingView);
        }, 60);
    }

    if (button == entryExpanded) {
        //if this was the one we close, leave it
        entryExpanded = false;
    }
    else {
        //open the new one
        entryExpanded = button;
        //$.situationScroller.height = Ti.UI.FILL;
        addSkillsToExpandedEntry();
        expandAnimation = Titanium.UI.createAnimation({
            height: containerView.calculatedHeight,
            duration: 125
        });

        entryExpanded.animate(flipAnimation);
        entryExpanded.parent.animate(expandAnimation);


    }
    event.cancelBubble = true;
}

function removeAllSkillSituationMaps(skillSituationMap) {
    var maps = skillSituationMap.toArray();
    for (var x = 0; x < maps.length; ++x) {
        skillSituationMap.remove(maps[x]);
        maps[x].destroy();
    }
}

function addSkillsToExpandedEntry() {
    Ti.API.info("Showing skills");
    var situationView = entryExpanded.parent;
    containerView = Ti.UI.createView({
        layout: "vertical",
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        left: 10,
        top: 50
    });

    containerView.addEventListener('click', function(event) {
        event.cancelBubble = true;
    });

    var allSkills = skillCollection.toArray();

    if (allSkills.length == 0) {
        containerView.add(Ti.UI.createLabel({
            text: "You don't have any Skills created yet.  Create a skill first, then come back here tell us to activate a skill reminder when this situation is activated."
        }));
        containerView.calculatedHeight = 50 + 13 + 66;
    }
    else {
        containerView.add(Ti.UI.createLabel({
            text: "Checking a skill will enable reminders when this situation is activated",
            font: { fontSize: 10 },
            color: "black"
        }));
        containerView.calculatedHeight = 50 + 13 + 33 * allSkills.length;
    }

    for (var i = 0; i < allSkills.length; ++i) {
        var skillName = allSkills[i].get("name");
        var mapValue = {
            skillId: allSkills[i].get("skillId"),
            situationId: situationView.situationId
        };

        var mapExists = (skillSituationMap.where(mapValue).length > 0);
        Ti.API.info(mapValue.situationId + "-" + mapValue.skillId + ": " + mapExists);

        var skillMapSwitch = Ti.UI.createSwitch({
            style: Ti.UI.Android.SWITCH_STYLE_CHECKBOX,
            value: mapExists,
            color: "black"
        });
        var skillLabel = Ti.UI.createLabel({
            text: skillName,
            width: Ti.UI.SIZE,
            height: Ti.UI.SIZE,
            left: 0,
            color: "black"
        });
        var skillMappingRow = Ti.UI.createView({
            layout: "horizontal",
            width: Ti.UI.FILL,
            height: Ti.UI.SIZE,
            top: 0,
            left: 0,
            skillId: mapValue.skillId,
            situationId: mapValue.situationId
        });

        skillMapSwitch.addEventListener("change", changeSkillMapSwitch);
        skillMapSwitch.addEventListener("click", cancelOp);
        skillLabel.addEventListener("click", clickSkillLabel);
        skillMappingRow.addEventListener("click", clickSkillRow);
        skillMappingRow.switch = skillMapSwitch;

        skillMappingRow.add(skillMapSwitch);
        skillMappingRow.add(skillLabel);
        containerView.add(Ti.UI.createView(subLineViewDefinition));
        containerView.add(skillMappingRow);
    }
    situationView.add(containerView);
}

function cancelOp(event) {
    event.cancelBubble = true;
}

function clickSkillLabel(event) {
    var skillMappingRow = event.source.parent;
    skillMappingRow.switch.value = !skillMappingRow.switch.value;
    event.cancelBubble = true;
}

function clickSkillRow(event) {
    var skillMappingRow = event.source;
    skillMappingRow.switch.value = !skillMappingRow.switch.value;
    event.cancelBubble = true;
}

function changeSkillMapSwitch(event) {
    var skillMappingRow = event.source.parent;
    var isNowMapped = skillMappingRow.switch.value;
    event.cancelBubble = true;
    Ti.API.info("Setting: " + skillMappingRow.situationId + "-" + skillMappingRow.skillId + ": " + isNowMapped);
    //do the db stuff to create the mapping
    var data = {
        situationId: skillMappingRow.situationId,
        skillId: skillMappingRow.skillId
    };
    if (isNowMapped) {
        var newDbMapping = Alloy.createModel('skillSituation', data);
        newDbMapping.save();
        skillSituationMap.add(newDbMapping);
    }
    else {
        var oldDbMapping = skillSituationMap.where(data)[0];
        skillSituationMap.remove(oldDbMapping);
        oldDbMapping.destroy();
    }
}

function onSituationDetailClose() {
    situationCollection.fetch();
}

//function resetBookList() {
//    var collection = Alloy.Collections.situation;
//    _.invoke(collection.toArray(), 'destroy');
//}

function preventOtherClicks(event) {
    event.cancelBubble = true;
}

function showLoading() {
    Ti.API.info("...........LOADING..........");
    if (!setupLoader) {
        $.loaderImage.setDuration(75);
        $.loaderImage.images = [
            '/images/loader/loader-1.png',
            '/images/loader/loader-2.png',
            '/images/loader/loader-3.png',
            '/images/loader/loader-4.png',
            '/images/loader/loader-5.png',
            '/images/loader/loader-6.png',
            '/images/loader/loader-7.png',
            '/images/loader/loader-8.png',
            '/images/loader/loader-9.png',
            '/images/loader/loader-10.png',
            '/images/loader/loader-11.png',
            '/images/loader/loader-12.png'];
        setupLoader = true;
    }
    $.loaderImage.start();
    $.loaderCover.show();
}

function hideLoading() {
    Ti.API.info("...........NOT  LOADING..........");
    $.loaderCover.hide();
    $.loaderImage.stop();
}

function clearData() {
    Ti.API.info("This feature is currently unsupported");
    alert("Not supported yet - delete the situations one by one.");
    var skillToReset = Alloy.Collections.skill.where({skillId: 1})[0];
    skillToReset.set("lastAlarm", 0);
    skillToReset.save();
}

exports.handleAdd = addSituation;
exports.handleReset = clearData;
