/**
 * Created by kaychoro on 2/17/15.
 */
var args = arguments[0] || {};

const SELECTED = "#0033CC";
const NOT_SELECTED = 'white';

var ignoreNextClickEvent = false;
var longSelecting = false;
var longSelectTime = false;
var priorScrollShutoff = false;
var longSelectData = {
    startingX: 0,
    startingY: 0,
    height: 0,
    width: 0,
    scaleFactor: 0
};

function initializeData() {
    var model = args;
    Ti.API.info("recurringtime.initializeData: " + JSON.stringify(model));
    for (var id in model) {
        if (model.hasOwnProperty(id) && $[id]) {
            setState($[id], true);
        }
    }
}

function clickHour(event) {
    if (!longSelecting && !ignoreNextClickEvent) {
        var backgroundColor = event.source.backgroundColor;
        var selectTime = (!backgroundColor || backgroundColor == NOT_SELECTED);
        setState(event.source, selectTime);
    }
    Ti.API.info("clickHour: " + ignoreNextClickEvent);
    ignoreNextClickEvent = false;
}

function longPressCal(event) {
    var source = event.source;
    //if (source.getSize())
    //if (event.)
}

function scrollListener(event) {
    $.scroller.setShowVerticalScrollIndicator(true);
    if (priorScrollShutoff) {
        clearTimeout(priorScrollShutoff);
        priorScrollShutoff = setTimeout(function () {
            $.scroller.setShowVerticalScrollIndicator(false);
            priorScrollShutoff = false;
        }, 3000);
    }
}

function selectMultipleHours(event) {
    Ti.API.info("longpress");
    var source = event.source;
    var backgroundColor = source.backgroundColor;
    var selectTime = (!backgroundColor || backgroundColor == NOT_SELECTED);
    $.scroller.setScrollingEnabled(false);
    longSelectTime = selectTime;
    longSelecting = true;
    var touchPoint = getTouchPoint(event);
    longSelectData.startingX = touchPoint.x;
    longSelectData.startingY = touchPoint.y;
    longSelectData.height = source.rect.height;
    longSelectData.width = source.rect.width;
    longSelectData.xBuffer = longSelectData.width * .4;
    longSelectData.yBuffer = longSelectData.height * .3;
    setState(source, selectTime);
}

function touchMoveHour(event) {
    if (longSelecting) {
        //Ti.API.info("touchmove");
        var source = event.source;
        var touchPoint = getTouchPoint(event);
        if (touchPoint.x < 0) {
            touchPoint.x += longSelectData.xBuffer;
        }
        else if (touchPoint.x > 0) {
            touchPoint.x -= longSelectData.xBuffer;
        }
        if (touchPoint.y < 0) {
            touchPoint.y += longSelectData.yBuffer;
        }
        else if (touchPoint.y > 0) {
            touchPoint.y -= longSelectData.yBuffer;
        }
        var xOffset = Math.floor((longSelectData.startingX + touchPoint.x) / longSelectData.width);
        var yOffset = Math.floor((longSelectData.startingY + touchPoint.y) / longSelectData.height);
        var sourceId = source.id;
        var idSplit = sourceId.split("_");

        var idY = parseInt(idSplit[1], 10);
        var idX = parseInt(idSplit[2], 10);

        var destinationY = Math.min(Math.max(idY + yOffset, 0), 47);
        var destinationX = Math.min(Math.max(idX + xOffset, 0), 6);

        var idPrint = "idY: " + idY + "   idX: " + idX;


        Ti.API.info("xOffset: " + xOffset + "   yOffset: " + yOffset);
        Ti.API.info(idPrint);
        //Ti.API.info("total x,y: " + totalX + "," + totalY);
        setState($["hour_" + destinationY + "_" + destinationX], longSelectTime);
    }
}

function touchEndHour(event) {
    if (longSelecting) {
        var touchPoint = getTouchPoint(event);
        var xOffset = Math.floor((longSelectData.startingX + touchPoint.x) / longSelectData.width);
        var yOffset = Math.floor((longSelectData.startingY + touchPoint.y) / longSelectData.height);
        Ti.API.info("touchend: " + xOffset + "," + yOffset);

        if (xOffset == 0 && yOffset == 0) {
            ignoreNextClickEvent = true;
        }

        longSelecting = false;
        $.scroller.setScrollingEnabled(true);
    }
    Ti.API.info("touchend");
}

function getTouchPoint(event) {
    if (OS_IOS) {
        return { x: event.x, y: event.y };
    }
    else if (OS_ANDROID) {
        return { x: event.x / Ti.Platform.displayCaps.logicalDensityFactor,
                y: event.y / Ti.Platform.displayCaps.logicalDensityFactor };
    }
}

function setState(destination, selected) {
    destination.backgroundColor = (selected ? SELECTED : NOT_SELECTED);
}

function getState(destination) {
    return destination.backgroundColor && destination.backgroundColor == SELECTED;
}

function getData() {
    var model = {};
    for (var hour = 0; hour < 48; hour++) {
        for (var day = 0; day < 7; day++) {
            var id = ("hour_" + hour + "_" + day);
            if (getState($[id])) {
                model[id] = true;
            }
        }
    }
    return model;
}

exports.getData = getData;
initializeData();