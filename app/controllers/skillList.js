var args = arguments[0] || {};

var situationCollection = Alloy.Collections.situation;
var skillModel = Alloy.Collections.skill;
var skillSituationMap = Alloy.Collections.skillSituation;
situationCollection.fetch();
skillModel.fetch();
skillSituationMap.fetch();

var subLineViewDefinition = {
    width: Ti.UI.FILL,
    right: 0,
    height: .5,
    backgroundColor: "gray"
};

var setupLoader = false;

var expandAnimation = Titanium.UI.createAnimation({
    height: "300",
    duration: 125
});
var contractAnimation = Titanium.UI.createAnimation({
    height: "41",
    duration: 125
});
var flippedMatrix = Ti.UI.create2DMatrix({
    rotate: 180
});
var flipAnimation = Ti.UI.createAnimation({
    transform: flippedMatrix,
    duration: 125
});
var unflippedMatrix = Ti.UI.create2DMatrix({
    rotate: 0
});
var unflipAnimation = Ti.UI.createAnimation({
    transform: unflippedMatrix,
    duration: 125
});
var entryExpanded = false;
var containerView = false;

//$.mainWin.open();

function doButtonClick(event) {
    var button = event.source;
    //close prior one
    if (entryExpanded && entryExpanded != null) {
        entryExpanded.animate(unflipAnimation);
        var priorExpandedSkillRow = entryExpanded.parent;
        priorExpandedSkillRow.animate(contractAnimation);
        //$.situationScroller.height = Ti.UI.SIZE;
        var priorSituationSkillMappingView = containerView;
        setTimeout(function(){
            priorExpandedSkillRow.remove(priorSituationSkillMappingView);
        }, 60);
    }

    if (button == entryExpanded) {
        //if this was the one we close, leave it
        entryExpanded = false;
    }
    else {
        //open the new one
        entryExpanded = button;
        //$.situationScroller.height = Ti.UI.FILL;
        addSituationsToExpandedEntry();
        expandAnimation = Titanium.UI.createAnimation({
            height: containerView.calculatedHeight,
            duration: 125
        });

        entryExpanded.animate(flipAnimation);
        entryExpanded.parent.animate(expandAnimation);


    }
    event.cancelBubble = true;
}



function addSituationsToExpandedEntry() {
    Ti.API.info("Showing situtaion");
    var skillView = entryExpanded.parent;
    containerView = Ti.UI.createView({
        layout: "vertical",
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        left: 10,
        top: 50
    });

    containerView.addEventListener('click', function(event) {
        event.cancelBubble = true;
    });

    var allSituations = situationCollection.toArray();

    if (allSituations.length == 0) {
        containerView.add(Ti.UI.createLabel({
            text: "You don't have any Situations created yet."
        }));
        containerView.calculatedHeight = 50 + 13 + 66;
    }
    else {
        containerView.add(Ti.UI.createLabel({
            text: "Checking a Situation will enable reminders when this situation happens.",
            font: { fontSize: 10 },
            color: "black"
        }));
        containerView.calculatedHeight = 50 + 13 + 33 * allSituations.length;
    }

    for (var i = 0; i < allSituations.length; ++i) {
        var situationName = allSituations[i].get("name");
        var mapValue = {
            skillId: skillView.skillId,
            situationId: allSituations[i].get("situationId")
        };
        
        var mapExists = (skillSituationMap.where(mapValue).length > 0);
        Ti.API.info(mapValue.situationId + "-" + mapValue.skillId + ": " + mapExists);

        var situationMapSwitch = Ti.UI.createSwitch({
            style: Ti.UI.Android.SWITCH_STYLE_CHECKBOX,
            value: mapExists,
            color: "black"
        });
        var situationLabel = Ti.UI.createLabel({
            text: situationName,
            width: Ti.UI.SIZE,
            height: Ti.UI.SIZE,
            left: 0,
            color: "black"
        });
        var situationMappingRow = Ti.UI.createView({
            layout: "horizontal",
            width: Ti.UI.FILL,
            height: Ti.UI.SIZE,
            top: 0,
            left: 0,
            skillId: mapValue.skillId,
            situationId: mapValue.situationId
        });

        situationMapSwitch.addEventListener("change", changeSkillMapSwitch);
        situationMapSwitch.addEventListener("click", cancelOp);
        situationLabel.addEventListener("click", clickSkillLabel);
        situationMappingRow.addEventListener("click", clickSkillRow);
        situationMappingRow.switch = situationMapSwitch;

        situationMappingRow.add(situationMapSwitch);
        situationMappingRow.add(situationLabel);
        containerView.add(Ti.UI.createView(subLineViewDefinition));
        containerView.add(situationMappingRow);
    }
    skillView.add(containerView);
}

function cancelOp(event) {
    event.cancelBubble = true;
}

function clickSkillLabel(event) {
    var situationMappingRow = event.source.parent;
    situationMappingRow.switch.value = !situationMappingRow.switch.value;
    event.cancelBubble = true;
}

function clickSkillRow(event) {
    var situationMappingRow = event.source;
    situationMappingRow.switch.value = !situationMappingRow.switch.value;
    event.cancelBubble = true;
}

function changeSkillMapSwitch(event) {
    var situationMappingRow = event.source.parent;
    var isNowMapped = situationMappingRow.switch.value;
    event.cancelBubble = true;
    Ti.API.info("Setting: " + situationMappingRow.situationId + "-" + situationMappingRow.skillId + ": " + isNowMapped);
    //do the db stuff to create the mapping

    var data = {
        situationId: situationMappingRow.situationId,
        skillId: situationMappingRow.skillId
    };
    if (isNowMapped) {
        var newDbMapping = Alloy.createModel('skillSituation', data);
        newDbMapping.save();
        skillSituationMap.add(newDbMapping);
    }
    else {
        var oldDbMapping = skillSituationMap.where(data)[0];
        skillSituationMap.remove(oldDbMapping);
        oldDbMapping.destroy();
    }
}





function clickNew()
{  
 var args = {};
   var addSkillController = Alloy.createController("skillview", args);
   var addSkillView = addSkillController.getView();
   addSkillView.open(); 
}


function openSkill(event)
	{
    var selectedSkill = event.source;
    
    var args = [];
    var skillFetch = skillModel.where({name: selectedSkill.text})[0];
    args[0] =  skillFetch.get('name');
    args[1] =  skillFetch.get('question');
    args[2] =  skillFetch.get('desiredAnswer') ;
    args[3] =  skillFetch.get('responseType') ;
    args[4] =  skillFetch.get('frequencyEvery') ;
    args[5] =  skillFetch.get('frequencyUnit') ;
    args[6] =  skillFetch.get('vibration') ;
    args[7] =  skillFetch.get('soundName') ;
    args[8] =  skillFetch.get('skillId');
   // $.mainWin.close();
    var editSkillController = Alloy.createController("skillview", args);
    var editSkillView = editSkillController.getView();
    editSkillView.open(); 

	}
	
/*	
	var selectedSituation = event.source;
	
    var situationModel = allSituations.where({name: selectedSituation.name})[0];
    var args = { data: situationModel.get('data') };
*/

function deleteEverything()
{         
          skillSituationMap.fetch();
	    _.invoke(skillModel.toArray(), 'destroy');
	    _.invoke(situationCollection.toArray(), 'destroy');
	    _.invoke(skillSituationMap.toArray(), 'destroy');
	    	
}



function resetSkills()
{
    _.invoke(skillModel.toArray(), 'destroy');
}

exports.handleAdd = clickNew;
exports.handleReset = resetSkills;

