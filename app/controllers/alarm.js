var arg = arguments[0] || {};

const TAP_ENABLED = true;

var closeAlarmWithoutFeedbackTimer;
var homePoint;
var moving = false;
var ONE_THIRD = 1 / 3;
var TWO_THIRDS = 2 / 3;

var TWO_FIFTHS = 2 / 5;
var THREE_FIFTHS = 3 / 5;

var absoluteCenter;
var upRest;
var downRest;
var fullHeight;
var returnAnimation = Ti.UI.createAnimation();
returnAnimation.duration = 100;
var moveToRestAnimation = Ti.UI.createAnimation();
moveToRestAnimation.duration = 100;
var Vibration1 = [0,10,10,10,1000,1000,500,1000,50];
var Vibration2 = [0,700,500,700,50,700,600,1000,500,50,1000,50,50,50];
var Vibration3 = [0,50,50,50,50,50,50,50,505,200,50,50,50,50,50,50,505,50];

function startPosition(e) {
    var yCoord = e.y / 2;
    fullHeight = $.touchLayer.rect.height;
    var pos = yCoord / fullHeight;
    absoluteCenter = fullHeight / 2;
    upRest = absoluteCenter * 2 / 7;
    downRest = fullHeight - upRest;
    returnAnimation.center = { x: 0, y: absoluteCenter };
    if (pos > ONE_THIRD && pos < TWO_THIRDS) {
        moving = true;
        clearTimeout(closeAlarmWithoutFeedbackTimer);
    }
    homePoint = yCoord;
}

function movingPosition(e) {
    if (moving) {
        var yCoord = e.y / 2;
        var delta = yCoord - homePoint;
        var newYPosition = absoluteCenter + delta;
        var positionPercentage = newYPosition / fullHeight;

        if (positionPercentage < ONE_THIRD || positionPercentage > TWO_THIRDS) {
            $.questionView.backgroundColor = "#88CCCCCC";
            $.questionText.color = "darkgray";
        }
        else {
            $.questionView.backgroundColor = "#DD000000";
            $.questionText.color = "white";
        }

        $.questionView.center = {x: 0, y: newYPosition };
        //Ti.API.info(yCoord + "           Delta: " + delta);
    }
}

function resetTimer() {
    clearTimeout(closeAlarmWithoutFeedbackTimer);
    closeAlarmWithoutFeedbackTimer = setTimeout(closeAlarm, 20000);
}

function returnQuestionView() {
    $.questionView.backgroundColor = "#DD000000";
    $.questionText.color = "white";
    $.questionView.animate(returnAnimation);
}

function finishUp(mechanism) {
    moveToRestAnimation.center = {x: 0, y: upRest};
    $.questionView.animate(moveToRestAnimation);
    closeAlarm($.upResponse.text, mechanism);
    setTimeout(returnQuestionView, 500);
}

function finishDown(mechanism) {
    moveToRestAnimation.center = {x: 0, y: downRest};
    $.questionView.animate(moveToRestAnimation);
    closeAlarm($.downResponse.text, mechanism);
    setTimeout(returnQuestionView, 500);
}

function endMove(e) {
    if (moving) {
        var yCoord = e.y / 2;
        var delta = yCoord - homePoint;
        var newYPosition = absoluteCenter + delta;
        $.questionView.center = {x: 0, y: newYPosition };
        var positionPercentage = newYPosition / fullHeight;

        if (positionPercentage <= ONE_THIRD) {
            finishUp("swipeUp");
        }
        else if (positionPercentage >= TWO_THIRDS) {
            finishDown("swipeDown");
        }
        else {
            returnQuestionView();
        }
    }
    moving = false;
}

$.touchLayer.addEventListener('touchstart', startPosition);
$.touchLayer.addEventListener('touchmove', movingPosition);
$.touchLayer.addEventListener('touchend', endMove);


var priorZ;
var highZState = false;
var consecutiveHighStateCount = 0;
var spaceCount = 0;
var taps = 0;

var zValues = [];
var zMean = 0;
const zParts = 20;
var counts = [];

function zAccelerometerAverager(e) {
    var zMeanPart = e.z / zParts;
    if (zValues.length < zParts) {
        zValues.push(zMeanPart);
        zMean += zMeanPart;
        return;
    }

    var zMeanDelta = e.z - zMean;
    var zDeltaDelta = zMeanDelta - priorZ;
    priorZ = zMeanDelta;

    if (Math.abs(zMeanDelta) >= 1.9 && Math.abs(zDeltaDelta) > 3) {
        counts.push("high");
        Ti.API.info("mean: " + zMean + "    \tdelta: " + zMeanDelta + "   \tdeltaDelta: " + zDeltaDelta + (Math.abs(zDeltaDelta) > 3 ? "     <---- TAP" : ""));
        highZState = true;
        if (!highZState) {
            consecutiveHighStateCount = 1;
        }
        spaceCount = 0;
        consecutiveHighStateCount++;
    }
    else {
        counts.push("low");
        //update the averages in the down state
        zValues.push(zMeanPart);
        zMean += zMeanPart;
        zMean -= zValues.shift();

        if (highZState) {
            //Ti.API.info("DOWNSTATE " + zDeltaNormalized + "\tTAP\t" + tapCount);
            Ti.API.info("DOWNSTATE mean: " + zMean + "    \tdelta: " + zMeanDelta + "   \tdeltaDelta: " + zDeltaDelta + "\tConsecutive high states:\t" + consecutiveHighStateCount);
            if (consecutiveHighStateCount > 0) {
                taps++;
            }
        }
        else {
            if (spaceCount > 10) {
                counts = [];
                //Ti.API.info("DOWNSTATE mean: " + zMean + "    \tdelta: " + zMeanDelta + "   \tdeltaDelta: " + zDeltaDelta + "\tSPACE\t" + spaceCount);
                //exceeded space iteration count... you're probably done tapping
                if (taps > 0) {
                    endTap();
                }
                taps = 0;
                spaceCount = 0;
            }
        }
        spaceCount++;
        highZState = false;
        consecutiveHighStateCount = 0;
    }
}

function endTap() {
    Ti.API.info("Taps: " + taps);
    if (taps == 2) {
        vibrateTwice();
        finishUp("tap 2");
    }
    else if (taps == 3) {
        vibrateThrice();
        finishDown("tap 3");
    }
}

const VIBRATE_INTERVAL = 150;
const SILENT_INTERVAL = 80;
function vibrateTwice() {
    Ti.Media.vibrate([0, VIBRATE_INTERVAL, SILENT_INTERVAL, VIBRATE_INTERVAL]);
}

function vibrateThrice() {
    Ti.Media.vibrate([0, VIBRATE_INTERVAL, SILENT_INTERVAL, VIBRATE_INTERVAL, SILENT_INTERVAL, VIBRATE_INTERVAL]);
}

function setAlarm(newAlarmSettings) {
    $.alarm.title = newAlarmSettings.skillName;
    $.questionText.text = newAlarmSettings.question;
    $.upResponse.text = newAlarmSettings.upResponse;
    $.downResponse.text = newAlarmSettings.downResponse;
    var vibration = false;
    if (newAlarmSettings.vibration == "Vibration1") {
        vibration = Vibration1;
    }
    else if (newAlarmSettings.vibration == "Vibration2") {
        vibration = Vibration2;
    }
    else if (newAlarmSettings.vibration == "Vibration3") {
        vibration = Vibration3;
    }

    Ti.API.info("vibration: " + newAlarmSettings.vibration);
    if (vibration) {
        Ti.Media.vibrate(vibration);
    }

    var upColor = '#EE4E4E'; //red
    var downColor = '#4EEE4E';
    if (newAlarmSettings.desired == "UP") {
        upColor = '#4EEE4E';
        downColor = '#EE4E4E'; //red
    }

    $.upResponseView.backgroundGradient = {
        type: 'linear',
        startPoint: { x: '50%', y: '95%' },
        endPoint: { x: '50%', y: '0%' },
        colors: [ { color: 'white', offset: 0.0}, { color: upColor, offset: 15.0 } ]
    };

    $.downResponseView.backgroundGradient = {
        type: 'linear',
        startPoint: { x: '50%', y: '5%' },
        endPoint: { x: '50%', y: '100%' },
        colors: [ { color: 'white', offset: 0.0}, { color: downColor, offset: 15.0 } ]
    };

    $.questionView.center = {
        x: 0,
        y: 0
    };
    if (TAP_ENABLED) {
        Ti.Accelerometer.addEventListener('update', zAccelerometerAverager);
    }

    closeAlarmWithoutFeedbackTimer = setTimeout(closeAlarm, 20000);

}

function closeAlarm(response, mechanism) {
    if (TAP_ENABLED) {
        Ti.Accelerometer.removeEventListener('update', zAccelerometerAverager);
    }
    if (response) {
        Ti.API.info("Write '" + response + "' to response DB");
        var responseData = {
            skillName: $.alarm.title,
            time: new Date().toLocaleString(),
            response: response,
            mechanism: mechanism
        };

        var responseModel;
        responseModel = Alloy.createModel('responseLog', responseData);
        responseModel.save();
    }
    $.alarm.close();
}

exports.setAlarm = setAlarm;