var startingAnnotation = arguments[0] || {};
var initializedAnnotation = false;
var VIEW_DELTA = 0.02;

function focusOnAnnotationOrLastLocation() {
    if (!initializedAnnotation) {
        initializedAnnotation = true;
        if (startingAnnotation.latitude) {
            createAnnotation(startingAnnotation.latitude, startingAnnotation.longitude, "Here");
        }
    }

    var annotations = $.mapview.getAnnotations();
    if (annotations.length > 0) {
        var annotation = annotations[0];
        $.mapview.setLocation({
            latitude: annotation.getLatitude(),
            longitude: annotation.getLongitude(),
            animate: false,
            latitudeDelta: VIEW_DELTA,
            longitudeDelta: VIEW_DELTA});
    }
    else {
        Ti.Geolocation.getCurrentPosition(moveToCurrentLocationCallback);
    }
}

function searchAddress() {
    var addressToSearch = $.locationAddress.getValue();
    Ti.Geolocation.forwardGeocoder(addressToSearch, createAnnotationAtAddressCallback)
}

function activateKeyboard(event) {
    $.locationAddress.focus();
    event.cancelBubble = true;
}

function hideKeyboard(event) {
    $.locationAddress.blur();
    event.cancelBubble = true;
}

function createAnnotationAtAddressCallback(geocodeResponse) {
    if (!geocodeResponse.success) {
        alert("Could not find the address");
        return;
    }

    var lat = geocodeResponse.latitude;
    var long = geocodeResponse.longitude;
    createAnnotation(lat, long, "Here");
    $.mapview.setLocation({
        latitude: lat, longitude: long, animate: true,
        latitudeDelta: VIEW_DELTA, longitudeDelta: VIEW_DELTA});
}

function moveToCurrentLocationCallback(locationResult) {
    if (!locationResult.success) {
        return;
    }

    var lat = locationResult.coords.latitude;
    var long = locationResult.coords.longitude;
    $.mapview.setLocation({
        latitude: lat, longitude: long, animate: false,
        latitudeDelta: VIEW_DELTA, longitudeDelta: VIEW_DELTA});
}

function customClickOnAnnotation(event) {
    if (event.clicksource == "leftPane" || event.clicksource == "leftButton") {
        $.mapview.removeAnnotation(event.annotation)
    }
}

function annotationClickIOS() {
    Ti.API.info("Called ios annotation click");
}

function longMap(event) {
    //var annotation = Alloy.Globals.Map.createAnnotation({
    //    latitude: event.latitude,
    //    longitude: event.longitude,
    //    title: "Appcelerator Headquarters",
    //    subtitle: "Mountain View, CA",
    //    pincolor: Alloy.Globals.Map.ANNOTATION_RED
    //});
    //var deleteButton;
    //if (OS_ANDROID) {
    //    annotation.leftView = Ti.UI.createButton({
    //        image: "images/delete-button-th.png",
    //        backgroundColor: "transparent"
    //    });
    //}
    //else if (OS_IOS) {
    //    annotation.leftButton = "delete-button-th.png";
    //    annotation.rightButton = Ti.UI.iPhone.SystemButton.SAVE;
    //}
    //$.mapview.addAnnotation(annotation);
    //$.mapview.selectAnnotation(annotation);

}

function createAnnotationHere(event) {
    if (!Ti.Geolocation.getLocationServicesEnabled()) {
        $.locationSettingsDisabledDialog.show();
    }
    Ti.Geolocation.purpose = "Recieve User Location";
    Ti.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
    Ti.Geolocation.getCurrentPosition(createAnnotationAtLocationCallback)
}

function locationSettingChoice(event) {
    $.locationSettingsDisabledDialog.hide();
    if (!event.cancel) {
        //jump to Android location settings
        var intent = Ti.Android.createIntent({action: "android.settings.LOCATION_SOURCE_SETTINGS"});
        Ti.Android.currentActivity.startActivity(intent);
    }
}

function createAnnotationAtLocationCallback(locationResult) {
    for (var x in locationResult) {
        Ti.API.info(x + ": " + locationResult[x]);
    }

    if (!locationResult.success) {
        return;
    }

    var lat = locationResult.coords.latitude;
    var long = locationResult.coords.longitude;
    createAnnotation(lat, long, "Here");
    $.mapview.setLocation({
        latitude: lat, longitude: long, animate: true,
        latitudeDelta: VIEW_DELTA, longitudeDelta: VIEW_DELTA});
}

function createAnnotation(lat, long, title) {
    title = title || "Current Location";
    var annotation = Alloy.Globals.Map.createAnnotation({
        latitude: lat,
        longitude: long,
        title: title,
        pincolor: Alloy.Globals.Map.ANNOTATION_RED
    });
    var deleteButton;
    if (OS_ANDROID) {
        annotation.leftView = Ti.UI.createButton({
            image: "/images/delete-button-th.png",
            backgroundColor: "transparent"
        });
    }
    else if (OS_IOS) {
        annotation.leftButton = "delete-button-th.png";
        annotation.rightButton = Ti.UI.iPhone.SystemButton.SAVE;
    }
    $.mapview.removeAllAnnotations();
    $.mapview.addAnnotation(annotation);
}

function longAndroidMap(event) {
    if (!OS_ANDROID) {
        return;
    }
    var lat = event.latitude;
    var long = event.longitude;
    createAnnotation(lat, long, "Here");
    $.mapview.setLocation({
        latitude: lat,
        longitude: long,
        animate: true,
        latitudeDelta: VIEW_DELTA,
        longitudeDelta: VIEW_DELTA});

}

function getCurrentLocation() {
    var annotations = $.mapview.getAnnotations();
    if (annotations.length > 0) {
        var annotation = annotations[0];
        return {
            latitude: annotation.getLatitude(),
            longitude: annotation.getLongitude()
        };
    }
    return {};
}

exports.getCurrentLocation = getCurrentLocation;
