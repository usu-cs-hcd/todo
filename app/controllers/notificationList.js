/**
 * Created by kaychoro on 4/8/15.
 *
 */
function refreshData () {
    Alloy.Collections.responseLog.fetch();
}
refreshData();

function clearData() {
    var responseLog = Alloy.Collections.responseLog;
    while (responseLog.length > 0) {
        var model = responseLog.at(responseLog.length - 1);
        responseLog.remove(model);
        model.destroy();
    }
}

exports.refreshData = refreshData;
exports.handleReset = clearData;
