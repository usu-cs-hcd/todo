var args = arguments[0] || {};


var skillModel = Alloy.Collections.skill;
skillModel.fetch();
var nativecontrols = require('learnappcelerator.nativecontrols');

//controls
var editMode=false;
var desiredAnswerText='Yes';
var responseType='Sound';
var frequencyEvery = "10";
var frequencyUnit = "minutes";
var desiredSelectionView=null;
var imageview =null;
var imageDownIcon=null;
var imageUpIcon=null;
var selectionView_s=null;
var selectionView_v=null;
var playIcon=null;
var radiogroup=null;
var soundView=null;
var closeSoundChoices=null;
var vibrationIcon=null;
var responseTypeName=null;
var responseTypeName_v;
var vibrationChoices=null;
var vibrationView=null;
var closeVibrationChoices=null;
var slider=null;
var vibrationName='Vibration1';
var soundName='Tangy';
var hintView=null;

//properties of vibration recording
var timePeriodInMs = 5000;
var recordedPattern = [];
var finalPattern = [];
var enableVibrations = false;

//ti.Media.vibrate parameters
var Vibration1 = [0,10,10,10,1000,1000,500,1000,50];
var Vibration2 = [0,700,500,700,50,700,600,1000,500,50,1000,50,50,50];
var Vibration3 = [0,50,50,50,50,50,50,50,505,200,50,50,50,50,50,50,505,50];
var CustomVibration = [];
var checkString = 'Vibration';

var middleView = Ti.UI.createView({
	backgroundColor:'transparent',
	width:'500',
	height:'550',
	zIndex:30
});

	var sound = Ti.Media.createSound({	
		url:'/media/Tangy.mp3'
	});


//skillview back button control
$.skillview.addEventListener('android:back', function(){
$.skillview.close();
});


    closeSoundChoices = Ti.UI.createImageView({
	    image:'/images/close.png',
		backgroundColor:'transparent',
		width:'30',
		height:'30',
		right:'0',
		top:'0',
		zIndex:'15'
	});
  	
	closeSoundChoices.addEventListener('click', function(){
		$.skillview.remove(soundView);
		$.skillview.remove(middleView);
		$.mainView.opacity=1.0;
		//selectionView_s=null;
	});
	
    radiogroup = nativecontrols.createRadioGroup({
	width : Ti.UI.SIZE,
	height : Ti.UI.SIZE,
	top :0,
	left : 0,
	textColor : "",
	selectedIndex : 2,
	layoutType : "vertical",
	buttons : [{
		id : 1,
		text : 'Carbon'
	}, {
		id : 2,
		text : 'Drop'
	}, {
		id : 3,
		text : 'Tangy'
	},{
		id : 4,
		text : 'Short Notice'
	}]
});

radiogroup.addEventListener('change', function(e) {
	var urlString = "/media/" +e.result.value+ ".mp3";
	var sound = Ti.Media.createSound({	
		url:urlString
	});
    sound.play();
    soundName = e.result.value;
    $.responseTypeView.removeAllChildren();
    responseTypeName.text= soundName;
    selectionView_s.removeAllChildren();
   selectionView_s.add(playIcon);
   selectionView_s.add(responseTypeName);
   selectionView_s.addEventListener('click', displaySounds);
   $.ResponsePicker.setSelectedRow(0,0,null);
   $.responseTypeView.add($.labelResponseType);
   $.responseTypeView.add($.ResponsePicker); 
   $.responseTypeView.add(selectionView_s); 
    

});
	
	
function initDesiredAnswerControls()
{
	desiredAnswer = nativecontrols.createRadioGroup({
	width : '60%',
	height : Ti.UI.SIZE,
	left : 0,
	textColor : "",
	selectedIndex : 0,
	layoutType : "vertical",
	buttons : [{
		id : 1,
		text : 'Yes'
	}, {
		id : 2,
		text : 'No'
	}]
});

desiredAnswer.addEventListener('change', function(e){
	desiredAnswerText= e.result.value;
});


	
// volume up-down button images

imageUpIcon = Ti.UI.createImageView({
	height:'50%',
	width:Ti.UI.SIZE,
	image:'/images/upIcon.png',
});


imageDownIcon = Ti.UI.createImageView({
	height:'50%',
	width:Ti.UI.SIZE,
	image:'/images/downIcon.png',
	//bottom:0	
});

 imageview = Ti.UI.createView({
	 height:Ti.UI.SIZE,
	 width:'40%',
	 layout:'vertical',
	 right:0,
	 backgroundColor:'#686363'
});

imageview.add(imageUpIcon);
imageview.add(imageDownIcon);


desiredSelectionView = Ti.UI.createView({
	layout:'horizontal',
	width: Ti.UI.FILL,
	height: Ti.UI.FILL,
	backgroundColor:'#686363'	
});

desiredSelectionView.add(desiredAnswer);
desiredSelectionView.add(imageview);
$.desiredAnswerView.add(desiredSelectionView);
	
}




function initResponseTypeControls()
{
 selectionView_s = Ti.UI.createView({
   layout:'horizontal',
   left:65,
   top:10
    });
    
 selectionView_v = Ti.UI.createView({
   layout:'horizontal',
   left:65,
   top:10
     });   

                      
   playIcon = Ti.UI.createImageView({
   width:'20',
   height:'20',
   image:'/images/play.png'
     });
 responseTypeName = Ti.UI.createLabel({
    text:"   " + soundName,
    color:'#c4c1b8',
     });


 vibrationIcon = Ti.UI.createImageView({
   width:'20',
   height:'20',
   image:'/images/vibrationIcon.png'
     });
     
  responseTypeName_v = Ti.UI.createLabel({
    text:"   " + vibrationName,
    color:'#c4c1b8',
     });
	
}




function initSoundSelection()
{
	soundView = Ti.UI.createScrollView({ 
    contentWidth: 'auto',
    contentHeight: 'auto',
    showVerticalScrollIndicator: true,    
    showHorizontalScrollIndicator: false,
   /* height: '40%',
    width: '50%',
    zIndex:15,*/
    height: '200',
    width: '200',
    zIndex:50,
    top:100,
    backgroundColor:'#494946'
  });          
  
      
    closeSoundChoices = Ti.UI.createImageView({
	    image:'/images/close.png',
		backgroundColor:'transparent',
		width:'30',
		height:'30',
		right:'0',
		top:'0',
		zIndex:'15'
	});
	

	
	closeSoundChoices.addEventListener('click', function(){
		$.skillview.remove(soundView);
		$.skillview.remove(middleView);
		$.mainView.opacity=1.0;
		//selectionView_s=null;
	});
	
  soundView.add(closeSoundChoices);
  
    radiogroup = nativecontrols.createRadioGroup({
	width : Ti.UI.SIZE,
	height : Ti.UI.SIZE,
	top :0,
	left : 0,
	textColor : "",
	selectedIndex : 2,
	layoutType : "vertical",
	buttons : [{
		id : 1,
		text : 'Carbon'
	}, {
		id : 2,
		text : 'Drop'
	}, {
		id : 3,
		text : 'Tangy'
	},{
		id : 4,
		text : 'Short Notice'
	}]
});

radiogroup.addEventListener('change', function(e) {
//	var urlString 
	var urlString = "/media/" +e.result.value+ ".mp3";
	var sound = Ti.Media.createSound({	
		url:urlString
	});
    sound.play();
    soundName = e.result.value;
    $.responseTypeView.removeAllChildren();
    responseTypeName.text= soundName;
    selectionView_s.removeAllChildren();
   selectionView_s.add(playIcon);
   selectionView_s.add(responseTypeName);
   selectionView_s.addEventListener('click', displaySounds);
   $.ResponsePicker.setSelectedRow(0,0,null);
   $.responseTypeView.add($.labelResponseType);
   $.responseTypeView.add($.ResponsePicker); 
   $.responseTypeView.add(selectionView_s); 
   
});

	
}

function initVibrationSelection()
{
  vibrationChoices = nativecontrols.createRadioGroup({
	width : Ti.UI.SIZE,
	height : Ti.UI.SIZE,
	top :0,
	left : 0,
	textColor : "",
	selectedIndex : 0,
	layoutType : "vertical",
	buttons : [{
		id : 1,
		text : 'Vibration1'
	}, {
		id : 2,
		text : 'Vibration2'
	}, {
		id : 3,
		text : 'Vibration3'
	},{
		id : 4,
		text : 'Custom'
	}]
});


vibrationChoices.addEventListener('change', function(e) 
{
//    var VibName = e.result.value;    
    switch(e.result.id)
    {
    	case 1:
    	Ti.Media.vibrate(Vibration1);
    	break;
    	case 2:
    	Ti.Media.vibrate(Vibration2);
    	break;
    	case 3:
    	Ti.Media.vibrate(Vibration3);
    	break;
    	case 4:
    	customVibrationRecorder();
    	break;
    
    }	//Ti.API.info("invoke vibration view");
    
    vibrationName = e.result.value;
    $.responseTypeView.removeAllChildren();
    responseTypeName_v.text= "   " + vibrationName;
    selectionView_v.add(vibrationIcon);
    selectionView_v.add(responseTypeName_v);
   // selectionView_v.addEventListener('click', displayVibrations);
    $.ResponsePicker.setSelectedRow(0,1,null);
    $.responseTypeView.add($.labelResponseType);
    $.responseTypeView.add($.ResponsePicker); 
    $.responseTypeView.add(selectionView_v);    
      	    
});

vibrationView = Ti.UI.createScrollView({  
    contentWidth: 'auto',
    contentHeight: 'auto',
    showVerticalScrollIndicator: true,    
    showHorizontalScrollIndicator: false,
    height: '200',
    width: '200',
    zIndex:50,
    top:100,
    backgroundColor:'#494946'
  }); 


  
  
closeVibrationChoices = Ti.UI.createImageView({
	    image:'/images/close.png',
		backgroundColor:'transparent',
		width:'30',
		height:'30',
		right:'0',
		top:'0',
		zIndex:'15'
	});	
	
}


function displayVibrations()
   {  
    $.skillName.blur();
	$.skillQuestion.blur();
	
	vibrationView = Ti.UI.createScrollView({  
    contentWidth: 'auto',
    contentHeight: 'auto',
    showVerticalScrollIndicator: true,    
    showHorizontalScrollIndicator: false,
    height: '200',
    width: '200',
    zIndex:50,
    top:100,
    backgroundColor:'#494946'
  }); 
  
  
  
  
   closeVibrationChoices.addEventListener('click', function(){
   	    $.skillview.remove(vibrationView);
   	    $.skillview.remove(middleView);
		$.mainView.opacity=1.0;
		Ti.Media.vibrate([0,1]);
		//selectionView_v=null;  	
   });
   
	
    vibrationView.add(closeVibrationChoices);
	vibrationView.add(vibrationChoices);
	$.mainView.opacity=0.4 ;
	$.dummyTextField.focus();
	$.skillview.add(middleView);
    $.skillview.add(vibrationView);
  //  responseTypeName_v.text = soundName;  	
   	
   }


 function displaySounds()
  { 
    $.skillName.blur();
	$.skillQuestion.blur();
	
	
	soundView = Ti.UI.createScrollView({ 
    contentWidth: 'auto',
    contentHeight: 'auto',
    showVerticalScrollIndicator: true,    
    showHorizontalScrollIndicator: false,
   /* height: '40%',
    width: '50%',
    zIndex:15,*/
    height: '200',
    width: '200',
    zIndex:50,
    top:100,
    backgroundColor:'#494946'
  });          
            
    closeSoundChoices = Ti.UI.createImageView({
	    image:'/images/close.png',
		backgroundColor:'transparent',
		width:'30',
		height:'30',
		right:'0',
		top:'0',
		zIndex:'15'
	});
  	
	closeSoundChoices.addEventListener('click', function(){
		$.skillview.remove(soundView);
		$.skillview.remove(middleView);
		$.mainView.opacity=1.0;
		//selectionView_s=null;
	});
	
    radiogroup = nativecontrols.createRadioGroup({
	width : Ti.UI.SIZE,
	height : Ti.UI.SIZE,
	top :0,
	left : 0,
	textColor : "",
	selectedIndex : 2,
	layoutType : "vertical",
	buttons : [{
		id : 1,
		text : 'Carbon'
	}, {
		id : 2,
		text : 'Drop'
	}, {
		id : 3,
		text : 'Tangy'
	},{
		id : 4,
		text : 'Short Notice'
	}]
});

radiogroup.addEventListener('change', function(e) {
	var urlString = "/media/" +e.result.value+ ".mp3";
	var sound = Ti.Media.createSound({	
		url:urlString
	});
    sound.play();
    soundName = e.result.value;
    $.responseTypeView.removeAllChildren();
    responseTypeName.text= soundName;
    selectionView_s.removeAllChildren();
   selectionView_s.add(playIcon);
   selectionView_s.add(responseTypeName);
   selectionView_s.addEventListener('click', displaySounds);
   $.ResponsePicker.setSelectedRow(0,0,null);
   $.responseTypeView.add($.labelResponseType);
   $.responseTypeView.add($.ResponsePicker); 
   $.responseTypeView.add(selectionView_s); 
    

});
	
	
  	
  
 // soundView.add(closeSoundChoices);
  soundView.add(radiogroup);
  soundView.add(closeSoundChoices);
  $.mainView.opacity=0.4 ;
  $.skillview.add(middleView);
  $.skillview.add(soundView);
  $.dummyTextField.focus();
  responseTypeName.text = soundName;   
 }                  


/*
	function displaySelection()
    { 	
	if(selectionView_s!=null)
	{
		$.responseTypeView.remove(selectionView_s);
		selectionView_s = null;
    
                      
   selectionView_v.add(vibrationIcon);
   selectionView_v.add(responseTypeName_v);
   selectionView_v.addEventListener('click', displayVibrations);		
   $.responseTypeView.add(selectionView_v);			
	}	
	else if(selectionView_v!=null)
	{
		$.responseTypeView.remove(selectionView_v);
		selectionView_v = null;
		
   selectionView.add(playIcon);
   selectionView.add(responseTypeName);
   selectionView.addEventListener('click', displaySounds);
   $.responseTypeView.add(selectionView); 	
	}
	
}

*/


function displaySelection()
{ 	 
	//alert(JSON.stringify($.ResponsePicker.getSelectedRow(0)));
	if($.ResponsePicker.getSelectedRow(0).title=='Vibration')
	{  // alert('resetting vibration controls');
		$.responseTypeView.remove(selectionView_s);
	//	selectionView_s = null;
    
                      
   selectionView_v.add(vibrationIcon);
   selectionView_v.add(responseTypeName_v);
   selectionView_v.addEventListener('click', displayVibrations);		
   $.responseTypeView.add(selectionView_v);			
	}	
	else if($.ResponsePicker.getSelectedRow(0).title=='Sound')
	{ //  alert('resetting sound Control');
		$.responseTypeView.remove(selectionView_v);
	//	selectionView_v = null;
		
   selectionView_s.add(playIcon);
   selectionView_s.add(responseTypeName);
   selectionView_s.addEventListener('click', displaySounds);
   $.responseTypeView.add(selectionView_s); 	
	}
	
	//responseType = $.ResponsePicker.getSelectedRow(0).
	responseType = $.ResponsePicker.getSelectedRow(0).title;
}

/*

function recordWindow()
{
	recordView = Ti.UI.createView({
		height:'200',
		width:'200',
		zIndex:3,
		top:'100',
		backgroundColor:'#494946',
	});
	
	recordImageView = Ti.UI.createImageView({
		image:'/images/record.png',
		backgroundColor:'transparent',
		width:'150',
		height:'150',
		bottom:'50',
		zIndex:'3'
	});
	
	closeImageView = Ti.UI.createImageView({
	    image:'/images/close.png',
		backgroundColor:'transparent',
		width:'30',
		height:'30',
		right:'0',
		top:'3',
		zIndex:'3'
	});

	
	recordButton = Ti.UI.createButton({
		backgroundColor:'#787878',
		width:'100',
	    height:'50',
	    title:'Record',
	    bottom:'0',
	    color:'white',
	    left:'0',
	    borderColor:'white',
	    borderWidth:'1',
	    zIndex:'3'
	}); 

	
	playButton = Ti.UI.createButton({
		backgroundColor:'#787878',
	    height:'50',
		width:'100',
		title:'Play',
		color:'white',
		bottom:'0',
		right:'0',
		borderColor:'white',
	    borderWidth:'1',
	    zIndex:'3'
	});
	
	closeImageView.addEventListener('click', function(){
	$.mainView.remove(recordView);
	recordView=null;
});



function beginRecordingSoon() {
    recordButton.backgroundColor = "yellow";
    setTimeout(startRecording, 1000);
}

function startRecording() {
    var startTime = new Date().getTime();
    recordedPattern = [];
    recordedPattern.push(startTime);
    enableVibrations = true;
    recordButton.backgroundColor = "green";
    setTimeout(endRecording, timePeriodInMs);
}

function endRecording() {
    stopVibrating();
    enableVibrations = false;
    var endTime = new Date().getTime();
    recordedPattern.push(endTime);
    recordButton.backgroundColor = "#787878";

    var previousTime = recordedPattern[0];
    Ti.API.info("recorded pattern:");
    Ti.API.info(recordedPattern);
    finalPattern = [];
    for (var i = 1; i < recordedPattern.length; ++i) {
        var currentTime = recordedPattern[i];
        finalPattern.push(currentTime - previousTime);
        previousTime = currentTime;
    }
    Ti.API.info(finalPattern);

   // setTimeout(playbackFinal, 3000);
}

function playbackFinal() {
  //  $.recordButton.backgroundColor = "red";
    Ti.Media.vibrate(finalPattern);
   // $.recordButton.backgroundColor = "purple";
}

function startVibrating() {
    if (enableVibrations) {
        recordedPattern.push(new Date().getTime());
        Ti.Media.vibrate([0, timePeriodInMs]);
    }
}

function stopVibrating() {
    if (enableVibrations) {
        recordedPattern.push(new Date().getTime());
        Ti.Media.vibrate([0, 1]);
    }
}




    recordButton.addEventListener('click', beginRecordingSoon);
    recordImageView.addEventListener('touchstart', startVibrating);
    recordImageView.addEventListener('touchend', stopVibrating);
	playButton.addEventListener('click', playbackFinal);
	
/*	buttonView.add(recordButton);
	buttonView.add(playButton);
	// * /
	recordView.add(closeImageView);
	recordView.add(recordImageView);
	recordView.add(playButton);
	recordView.add(recordButton);
	$.mainView.add(recordView);
	 	
}


*/



function customVibrationRecorder()
 {  
 	Ti.Media.vibrate([0,1]);
 	
   	recordView = Ti.UI.createView({
   		id:'recordView',
		height:'200',
		width:'200',
		zIndex:50,
		top:'0',
		left:'0',
		backgroundColor:'#494946',
	});
	
	recordImageView = Ti.UI.createImageView({
		image:'/images/record.png',
		backgroundColor:'transparent',
		width:'150',
		height:'150',
		bottom:'50',
		zIndex:50
	});
	
	closeImageView = Ti.UI.createImageView({
	    image:'/images/close2.png',
		backgroundColor:'transparent',
		width:'30',
		height:'30',
		right:'0',
		top:'0',
		zIndex:50
	});
	

	
/*	buttonView = Ti.UI.createView({
		height:'50',
		width:'200',
		layout:'horizontal',
		zIndex:'3',
		bottom:'0',
		backgroundColor:'white'
}); */
	
	recordButton = Ti.UI.createButton({
		backgroundColor:'#787878',
		width:'33%',
	    height:'50',
	    title:'Record',
	    bottom:'0',
	    color:'white',
	    left:'0',
	    borderColor:'white',
	    borderWidth:'1',
	    zIndex:50
	}); 

	
	playButton = Ti.UI.createButton({
		backgroundColor:'#787878',
	    height:'50',
		width:'33%',
		title:'Play',
		color:'white',
		bottom:'0',
		borderColor:'white',
	    borderWidth:'1',
	    zIndex:50
	});
	
	
	saveButton = Ti.UI.createButton({
		backgroundColor:'#787878',
	    height:'50',
		width:'33%',
		title:'Save',
		color:'white',
		bottom:'0',
		right:'0',
		borderColor:'white',
	    borderWidth:'1',
	    zIndex:50
	});
	
	closeImageView.addEventListener('click', function(){
	//$.scrollView.remove(recordView);
	vibrationView.removeAllChildren();
    vibrationView.add(closeVibrationChoices);
	vibrationView.add(vibrationChoices);
	recordView=null; 
});



function beginRecordingSoon() {
	 hintView = Ti.UI.createLabel({
		top:0,
		zIndex:200,
		text:"Tap the center screen to vibrate when record button goes green.",
		backgroundColor:'Yellow',
		color:'Red',
		width:"100%",
		height:0		
	});
	 
	 var ani = Ti.UI.createAnimation({
	 	duration:1000,
	 	height:60
	 });
	 hintView.animate(ani);
	 $.mainView.add(hintView);
    recordButton.backgroundColor = "yellow";
    setTimeout(startRecording, 4000);
}

function startRecording() {
	$.mainView.remove(hintView);
    var startTime = new Date().getTime();
    recordedPattern = [];
    recordedPattern.push(startTime);
    enableVibrations = true;
    recordButton.backgroundColor = "green";
    setTimeout(endRecording, timePeriodInMs);
}

function endRecording() {
    stopVibrating();
    enableVibrations = false;
    var endTime = new Date().getTime();
    recordedPattern.push(endTime);
    recordButton.backgroundColor = "#787878";

    var previousTime = recordedPattern[0];
    Ti.API.info("recorded pattern:");
    Ti.API.info(recordedPattern);
    finalPattern = [];
    for (var i = 1; i < recordedPattern.length; ++i) {
        var currentTime = recordedPattern[i];
        finalPattern.push(currentTime - previousTime);
        previousTime = currentTime;
    }
  //  Ti.API.info(finalPattern);

   // setTimeout(playbackFinal, 3000);
}

function playbackFinal() {
  //  $.recordButton.backgroundColor = "red";
    Ti.Media.vibrate(finalPattern);
}

function startVibrating() {
    if (enableVibrations) {
        recordedPattern.push(new Date().getTime());
        Ti.Media.vibrate([0, timePeriodInMs]);
    }
}

function stopVibrating() {
    if (enableVibrations) {
        recordedPattern.push(new Date().getTime());
        Ti.Media.vibrate([0, 1]);
    }
}




    recordButton.addEventListener('click', beginRecordingSoon);
    recordImageView.addEventListener('touchstart', startVibrating);
    recordImageView.addEventListener('touchend', stopVibrating);
	playButton.addEventListener('click', playbackFinal);
	
	saveButton.addEventListener('click', function(){
		customVibration=finalPattern;
		Ti.API.info(customVibration);
	    vibrationView.removeAllChildren();
        vibrationView.add(closeVibrationChoices);
	    vibrationView.add(vibrationChoices);
	    recordView=null;
	});
	
/*	buttonView.add(recordButton);
	buttonView.add(playButton);
	*/
	vibrationView.removeAllChildren();
	recordView.add(closeImageView);
	recordView.add(recordImageView);
	recordView.add(recordButton);
    recordView.add(saveButton);
    recordView.add(playButton);
    vibrationView.add(recordView);
	//$.vibrationRecordView.add(recordView);
//	$.scrollView.add(recordView);
   	
   }
   

   function updateFrequency(e)
  {  
    	$.digitSliderLabel.text = parseInt(e.value);  
     	digitIndex = $.digitSliderLabel.text;	
     	frequencyEvery =  parseInt(e.value);      	
  }


function initSlider()
{
    slider = Titanium.UI.createSlider({
        min:1.0,
        max:59.0,
        value:frequencyEvery,
        width:270,
        height:'auto',
        top:355,
        left:60,
        zIndex:10,
        backgroundColor:'#494946'   
        });
        slider.addEventListener('change', updateFrequency);
	
}   
   
   
function displaySlider()
{   if(slider.width=='270' && slider.height=='auto')
	{
//	var t = Titanium.UI.create2DMatrix();
 //   t = t.rotate(-90);
   slider.height='0';
   slider.width='0';
    
    slider.addEventListener('change', function(e) {
    $.digitSliderLabel.text = parseInt(e.value);  
    digitIndex = $.digitSliderLabel.text;
    });
    
    $.mainView.add(slider);
}

else
  {
    slider.width='270';
    slider.height='auto'; 
  }

}


function changeSlider()
{
	if($.unitPicker.getSelectedRow(0).title=="hours")
	slider.max=23;
	else
	slider.max=59;
	
	frequencyUnit = $.unitPicker.getSelectedRow(0).title;
}   
   
function timeUnitChange(e)
{
	unitIndex = e.rowIndex;
}
 

function firstTimeResponseTypeDisplay()
{
	
   selectionView_s = Ti.UI.createView({
   layout:'horizontal',
   left:65,
   top:10
    });
    
   playIcon = Ti.UI.createImageView({
   width:'20',
   height:'20',
   image:'/images/play.png'
     });
    responseTypeName = Ti.UI.createLabel({
    text:"   " + soundName,
    color:'#c4c1b8'
     });
     
   selectionView_s.add(playIcon);
   selectionView_s.add(responseTypeName);
   selectionView_s.addEventListener('click', displaySounds);
   $.responseTypeView.add(selectionView_s);
	
}

function editModeSkill()
{
	editMode=true;
   Ti.API.info(JSON.stringify("Data recieved for Skill Edit Mode---" + args));
	$.skillview.open();
	$.skillName.value = args[0];
	$.skillQuestion.value = args[1];
	if(args[2]=="Yes") desiredAnswer.selectedIndex=0; 
	else if(args[2]=="No") desiredAnswer.selectedIndex=1;
	desiredAnswerText=args[2]; 
	if(args[3]=="Sound") $.ResponsePicker.setSelectedRow(0,0,null); 
	else if(args[3]=="Vibration") $.ResponsePicker.setSelectedRow(0,1,null);
	responseType = args[3];
	$.digitSliderLabel.text=args[4];
	frequencyEvery = args[4];
	if(args[5]=="minutes") $.unitPicker.setSelectedRow(0,0,null);
	else if(args[5]=="hours") $.unitPicker.setSelectedRow(0,1,null);
	frequencyUnit = args[5];
	if(args[6]!="null") vibrationName = args[6];
	if(args[7]!="null") soundName = args[7];  
	$.removeButton.width="80%";
	$.removeButton.height="40";
	$.removeButton.addEventListener('click', removeCurrentSkill);
//	Ti.API.info(JSON.stringify(desiredAnswer));
   /* 
	if(args[3]!=0)$.ResponsePicker.setSelectedRow(0,args[3],false);
	$.digitSliderLabel.text = args[4];
	$.unitPicker.setSelectedRow(0,args[5],false);
	*/
}



function removeCurrentSkill()
{
//   alert("entring remove mode");
	var dialog = Ti.UI.createAlertDialog({
    cancel: 1,
    buttonNames: ['Yes', 'No'],
    message: 'Do You want to delete this skill?',
    title: 'Delete'
    });
    
      dialog.addEventListener('click', function(e){
      if (e.index == e.source.Yes)
      {
      	Ti.API.info("Deleting skill");
    	var currentSkill = skillModel.where({skillId: args[8]})[0];
        skillModel.remove(currentSkill);
        currentSkill.destroy();
        $.skillview.close();   	
      } 
   });
   
        dialog.show(); 
} 
   


function saveSkill()
{	var newSkill;	
   if($.ResponsePicker.getSelectedRow(0).title=='Vibration')
     {
       newSkill = Alloy.createModel('skill', { 
       name: $.skillName.value,  
       question: $.skillQuestion.value ,
       desiredAnswer: desiredAnswerText ,
       responseType: responseType ,
       frequencyEvery: frequencyEvery ,
       frequencyUnit:  frequencyUnit,  
       vibration: vibrationName,
       soundName: null,
       lastAlarm:  new Date().getTime()
}); 
     }
else
     {
       newSkill = Alloy.createModel('skill', { 
       name: $.skillName.value,  
       question: $.skillQuestion.value ,
       desiredAnswer: desiredAnswerText ,
       responseType: responseType ,
       frequencyEvery: frequencyEvery ,
       frequencyUnit:  frequencyUnit,  
       vibration: null,
       soundName: soundName,
       lastAlarm: new Date().getTime()
}); 
     }
     
if(editMode==false)
{     
Ti.API.info("Saving New Skill---" + JSON.stringify(newSkill));
skillModel.add(newSkill);
newSkill.save(); 
}
else
{
	    var oldSkill = skillModel.where({skillId: args[8]})[0];
        oldSkill.set(newSkill);
        oldSkill.save();
}


$.skillview.close();
Ti.API.info("Skill Saved");

}



function initializeControls()
{
	
   initDesiredAnswerControls();
   initResponseTypeControls();	
   firstTimeResponseTypeDisplay();
   initSoundSelection();
   initSlider();
   initVibrationSelection(); 
   
   if(args.length>1)
   editModeSkill();	
   	
}




initializeControls();
