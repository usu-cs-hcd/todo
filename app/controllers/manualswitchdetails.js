/**
 * Created by kaychoro on 2/17/15.
 */
var startingValue = arguments[0] || false;

var on = startingValue;

function flip(event) {
    var touchPoint = getTouchPoint(event);
    var leftSideEvent = touchPoint.x < event.source.rect.width / 2;
    if (on && leftSideEvent) {
        on = false;
    }
    else if (!on && !leftSideEvent) {
        on = true;
    }
    updateImage();
}

function updateImage() {
    if (on) {
        $.image.image = "/images/manual_on.png";
        $.instructions.text = "This situation will remain active until switched off"
    }
    else {
        $.image.image = "/images/manual_off.png";
        $.instructions.text = "Flip the switch to enable this situation"
    }
}

function getTouchPoint(event) {
    if (OS_IOS) {
        return { x: event.x, y: event.y };
    }
    else if (OS_ANDROID) {
        return { x: event.x / Ti.Platform.displayCaps.logicalDensityFactor,
            y: event.y / Ti.Platform.displayCaps.logicalDensityFactor };
    }
}

function isOn() {
    return on;
}

exports.isOn = isOn;
updateImage();
