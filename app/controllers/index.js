var setupLoader = false;

if(OS_IOS) {
	$.navGroupWin.open();
}
if (OS_ANDROID) {
    $.index.title = "Situations";
    $.index.open();
}

var alarmController = Alloy.createController("alarm", {});
var alarmView = alarmController.getView();
alarmView.addEventListener("close", onAlarmClose);
var alarmViewIsOpen = false;
var alarmIteration = 0;

const MINUTES_MS = 60 * 1000;

function getArmedAlarm() {
    Ti.API.info("Checking alarms...");
    var skillCollection = Alloy.Collections.skill;
    var mappedSkillSituationCollection = Alloy.Collections.skillSituation;
    var situationCollection = Alloy.Collections.situation;
    var activeSituationIds = [];
    var allSituations = situationCollection.toArray();
    for (var i = 0; i < allSituations.length; ++i) {
        if (isSituationActive(allSituations[i])) {
            Ti.API.info(allSituations[i].get("name") + " is Active");
            var situationId = allSituations[i].get("situationId");
            activeSituationIds.push(situationId);
            var possibleSkillSituations = mappedSkillSituationCollection.where({situationId: situationId});
            for (var j = 0; j < possibleSkillSituations.length; ++j) {
                var skillId = possibleSkillSituations[j].get("skillId");
                var skill = skillCollection.where({skillId: skillId})[0];
                var frequency = Number(skill.get("frequencyEvery"));
                var lastOccurance;
                try {
                    lastOccurance = Number(skill.get("lastAlarm"));
                }
                catch (err) {
                    lastOccurance = 0;
                }
                var nextOccurance = (lastOccurance + (frequency * MINUTES_MS));
                Ti.API.info("Next Occurance: " + (nextOccurance - new Date().getTime()) + " milliseconds");

                if (nextOccurance <= new Date().getTime()) {
                    Ti.API.info(skill.get("name") + " ALARM is going to go off");
                    var desired = "DOWN";
                    if (skill.get("desiredAnswer") == "Yes") {
                        desired = "UP";
                    }
                    skill.set("lastAlarm", new Date().getTime());
                    skill.save();
                    return {
                        question: skill.get("question"),
                        upResponse: "Yes",
                        downResponse: "No",
                        desired: desired,
                        skillName: skill.get("name"),
                        vibration: skill.get("vibration")
                    };
                }
            }
        }
    }



    return false;
    var ready = (alarmIteration % 10 == 1);
    if (!ready) {
        return false;
    }
    return {
        question: "Have you kept your eyes open during class?",
        upResponse: "Yes",
        downResponse: "No",
        desired: "UP",
        skillName: "Open Eyes"
    };
}

function isSituationActive(situationModel) {
    //TODO activate by other means than manual - but only manual right now
    var parsedSituationDetails = JSON.parse(situationModel.get("data"));
    return parsedSituationDetails.activation == Alloy.Globals.SituationConstants.Types.MANUAL && parsedSituationDetails.manual;
}

function checkForAlarm() {
    alarmIteration++;
    var armedAlarm = getArmedAlarm();
    if (armedAlarm && !alarmViewIsOpen) {
        alarmViewIsOpen = true;
        alarmController.setAlarm(armedAlarm);
        if (OS_IOS) {
            $.navGroupWin.openWindow(alarmView);
        }
        if (OS_ANDROID) {
            alarmView.open();
        }
    }
}

function onAlarmClose() {
    Ti.API.info("Handle alarm close...");
    alarmViewIsOpen = false;
    $.notificationListId.refreshData();
}

var currentTabController;
$.situationsTab.addEventListener("focus", function() {
    $.index.title = "Situations";
    currentTabController = $.situationListId;
});
$.skillsTab.addEventListener("focus", function() {
    $.index.title = "Skills";
    currentTabController = $.skillListId;
});
$.notificationsTab.addEventListener("focus", function() {
    $.index.title = "Notifications";
    currentTabController = $.notificationListId;
});

var alarmInterval = setInterval(checkForAlarm, 5000);

function addNew() {
    currentTabController.handleAdd();
}

function resetList() {
    currentTabController.handleReset();
}


//Everything after here is for the vibration experiment


var timePeriodInMs = 5000;
var recordedPattern = [];
var finalPattern = [];

function beginRecordingSoon() {
    $.recordButton.backgroundColor = "yellow";
    setTimeout(startRecording, 2000)
}

function startRecording() {
    var startTime = new Date().getTime();
    recordedPattern = [];
    recordedPattern.push(startTime);
    enableVibrations = true;
    $.recordButton.backgroundColor = "green";
    setTimeout(endRecording, timePeriodInMs);
}

function endRecording() {
    stopVibrating();
    enableVibrations = false;
    var endTime = new Date().getTime();
    recordedPattern.push(endTime);
    $.recordButton.backgroundColor = "purple";

    var previousTime = recordedPattern[0];
    Ti.API.info("recorded pattern:");
    Ti.API.info(recordedPattern);
    finalPattern = [];
    for (var i = 1; i < recordedPattern.length; ++i) {
        var currentTime = recordedPattern[i];
        finalPattern.push(currentTime - previousTime);
        previousTime = currentTime;
    }
    Ti.API.info(finalPattern);

    setTimeout(playbackFinal, 3000);
}

function playbackFinal() {
    $.recordButton.backgroundColor = "red";
    Ti.Media.vibrate(finalPattern);
    $.recordButton.backgroundColor = "purple";
}

function startVibrating() {
    if (enableVibrations) {
        recordedPattern.push(new Date().getTime());
        Ti.Media.vibrate([0, timePeriodInMs]);
    }
}

function stopVibrating() {
    if (enableVibrations) {
        recordedPattern.push(new Date().getTime());
        Ti.Media.vibrate([0, 1]);
    }
}
