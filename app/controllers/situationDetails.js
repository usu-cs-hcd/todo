var args = arguments[0] || {};
var allSituations = Alloy.Collections.instance('situation');
var selectedSituationType = Alloy.Globals.SituationConstants.Types.LOCATION;
var currentSituationTypeView = null;
var activationTypeMap = {};
var timeRecurringMap = {};
var controllers = {};
var addingData = true;
var typeIcons = {};
typeIcons[Alloy.Globals.SituationConstants.Types.TIME] = "/images/small_time.png";
typeIcons[Alloy.Globals.SituationConstants.Types.LOCATION] = "/images/small_location.png";
typeIcons[Alloy.Globals.SituationConstants.Types.MANUAL + "_on"] = "/images/small_manual_on.png";
typeIcons[Alloy.Globals.SituationConstants.Types.MANUAL + "_off"] = "/images/small_manual_off.png";

// Default Values:
var dataModel = {
    name: "",
    activation: Alloy.Globals.SituationConstants.Types.MANUAL,
    time: {
        recurring: true,
        fromTime: new Date(),
        toTime: new Date(),
        schedule: {}
    },
    location: {},
    manual: false
};

const CLICKABLE_LINK_COLOR = "#007aff";
const PICKERS = {
    DATE: "DATE",
    TIME: "TIME"
};

function goBack() {
    Ti.API.info("Back Button pressed.");
    $.situationDetails.close();
}

function getIconImageType() {
    if (Alloy.Globals.SituationConstants.Types.MANUAL == selectedSituationType) {
        return typeIcons[selectedSituationType + (dataModel.manual ? "_on" : "_off")];
    }
    return typeIcons[selectedSituationType];
}

function saveSituation() {
    //Validations
    if ($.nameInput.value.trim().length == 0) {
        $.nameInput.hintText = "Name...     *Required";
        $.nameInput.focus();
        setTimeout(function() {
            $.nameInput.hintText = "Name...";
        }, 2000);
        return;
    }

    dataModel.name = $.nameInput.value.trim();
    dataModel.activation = selectedSituationType;
    dataModel.time.schedule = controllers.recurring.getData();
    dataModel.manual = controllers.manual.isOn();
    dataModel.location = controllers.location.getCurrentLocation();

    var dataModelText = JSON.stringify(dataModel);
    var imageName = getIconImageType();
    //Ti.API.info(dataModelText);
    var newData = {
        name: dataModel.name,
        image: imageName,
        data: dataModelText
    };

    var situation;
    if (addingData) {
        situation = Alloy.createModel('situation', newData);
        allSituations.add(situation);
    }
    else {
        situation = allSituations.where({situationId: dataModel.situationId})[0];
        situation.set(newData);
    }
    situation.save();
    $.situationDetails.close();
}

function initializeDataModel() {
    if (args.data) {
        dataModel = JSON.parse(args.data);
        dataModel.situationId = args.situationId;
        //Dates are not preserved in the JSON parsing, so we need to create dates from these
        dataModel.time.fromTime = new Date(dataModel.time.fromTime);
        dataModel.time.toTime = new Date(dataModel.time.toTime);
        addingData = false;
    }
    else if (dataModel.time.fromTime == dataModel.time.toTime) {
        dataModel.time.toTime.setHours(dataModel.time.fromTime.getHours() + 1);
    }

    $.nameInput.setValue(dataModel.name);
}

function initializeControllers() {
    controllers.recurring = Alloy.createController("recurringtime", dataModel.time.schedule);
    controllers.location = Alloy.createController("locationdetails", dataModel.location);
    controllers.manual = Alloy.createController("manualswitchdetails", dataModel.manual);

    //for (var x in controllers.recurring) {
    //    Ti.API.info("controllers.recurring[" + x + "]: " + controllers.recurring[x]);
    //}

    $.timeSwitch.situationType = Alloy.Globals.SituationConstants.Types.TIME;
    $.locationSwitch.situationType = Alloy.Globals.SituationConstants.Types.LOCATION;
    $.manualSwitch.situationType = Alloy.Globals.SituationConstants.Types.MANUAL;
}

function initializeViewConnections() {
    timeRecurringMap[PICKERS.DATE] = buildDatePicker();
    timeRecurringMap[PICKERS.TIME] = buildTimePicker();
    timeRecurringMap.recurring = controllers.recurring.getView();
    timeRecurringMap.oneTime = buildOneTimeView();
    activationTypeMap[Alloy.Globals.SituationConstants.Types.TIME] = buildTimeSettingView();
    activationTypeMap[Alloy.Globals.SituationConstants.Types.LOCATION] = controllers.location.getView();
    activationTypeMap[Alloy.Globals.SituationConstants.Types.MANUAL] = controllers.manual.getView();

    //show the delete button on edit, but not on add
    if (!addingData) {
        $.situationDetails.title = "Edit Situation";
        $.bottomContainer.show();
    }
}

function populateInitialView() {
    switch (dataModel.activation) {
        case Alloy.Globals.SituationConstants.Types.TIME:
            $.timeSwitch.value = true;
            break;
        case Alloy.Globals.SituationConstants.Types.LOCATION:
            $.locationSwitch.value = true;
            break;
        case Alloy.Globals.SituationConstants.Types.MANUAL:
            $.manualSwitch.value = true;
            break;
    }
    handleTypeChange({
        source: {
            value: true,
            situationType: dataModel.activation
        }
    });
}

function buildTimeSettingView() {
    var timeSettingView = Ti.UI.createView({
        height: Ti.UI.FILL,
        width: "100%",
        backgroundColor: "white",
        layout: "vertical"
    });
    var title = Ti.UI.createLabel({
        text: "Time Settings",
        top: 4,
        font: {fontSize: 12},
        color: "darkgray"
    });
    timeSettingView.add(title);

    var view = Ti.UI.createView({
        layout: "horizontal",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE
    });
    var recurringSwitchLabel = Ti.UI.createLabel({
        text: "Recurring Schedule",
        font: {fontSize: 14},
        color: "black",
        right: 20

    });
    var recurringSwitch = Ti.UI.createSwitch({
        id: "recurringSwitch",
        value: dataModel.time.recurring,
        left: 20
    });
    if (OS_ANDROID) {
        recurringSwitch.style = Ti.UI.Android.SWITCH_STYLE_CHECKBOX;
    }
    if (OS_IOS) {
    }
    recurringSwitchLabel.addEventListener("click", function() {
        recurringSwitch.value = !recurringSwitch.value;
    });
    recurringSwitch.addEventListener("change", handleTimeRecurringChange);
    view.add(recurringSwitchLabel);
    view.add(recurringSwitch);
    timeSettingView.add(view);
    timeSettingView.add(recurringSwitch.value ? timeRecurringMap.recurring : timeRecurringMap.oneTime);

    return timeSettingView;
}

function handleTypeChange(event) {
    $.nameInput.blur();
    var source = event.source;
    if (source.value) {
        selectedSituationType = source.situationType;
        highlightViews();
    }
}

function refreshOneTimeView() {
    timeRecurringMap.oneTime.fromDateLabel.text = dataModel.time.fromTime.toLocaleDateString();
    timeRecurringMap.oneTime.fromTimeLabel.text = formatTimeString(dataModel.time.fromTime);
    timeRecurringMap.oneTime.toDateLabel.text = dataModel.time.toTime.toLocaleDateString();
    timeRecurringMap.oneTime.toTimeLabel.text = formatTimeString(dataModel.time.toTime);
}

function removeCurrentSituation() {
    Ti.API.info("removing");
    var situation = allSituations.where({situationId: dataModel.situationId})[0];
    allSituations.remove(situation);
    situation.destroy();
    $.situationDetails.close();
}

function activateKeyboard(event) {
    $.nameInput.focus();
    event.cancelBubble = true;
}

function hideKeyboard() {
    $.nameInput.blur();
}

function applyPick(pickerLabel) {
    var values = closePicker(pickerLabel);
    if (pickerLabel === PICKERS.TIME) {
        dataModel.time[values.label].setHours(values.value.getHours());
        dataModel.time[values.label].setMinutes(values.value.getMinutes());
    }
    else {
        dataModel.time[values.label].setFullYear(values.value.getFullYear());
        dataModel.time[values.label].setMonth(values.value.getMonth());
        dataModel.time[values.label].setDate(values.value.getDate());
    }
    refreshOneTimeView();
}

function cancelPick(pickerLabel) {
    closePicker(pickerLabel);
}

function openPicker(pickerLabel, label) {
    var modalView = timeRecurringMap[pickerLabel];
    modalView.picker.value = dataModel.time[label];
    modalView.modelLabel = label;
    $.situationDetails.add(modalView);
}

function closePicker(pickerLabel) {
    var pickerInfo = {
            value: timeRecurringMap[pickerLabel].picker.value,
            label: timeRecurringMap[pickerLabel].modelLabel
        };
    $.situationDetails.remove(timeRecurringMap[pickerLabel]);
    return pickerInfo;
}

function closeTimePicker() {
    var dateChosen = timeRecurringMap.timePicker.picker.value;
    $.situationDetails.remove(timeRecurringMap.timePicker);
    return dateChosen;
}

function handleTimeRecurringChange(event) {
    Ti.API.info("Recurring change: " + event.source.value);
    dataModel.time.recurring = event.source.value;
    updateTimeSubView();
}

function updateTimeSubView() {
    if (dataModel.time.recurring) {
        currentSituationTypeView.remove(timeRecurringMap.oneTime);
        currentSituationTypeView.add(timeRecurringMap.recurring);
    }
    else {
        currentSituationTypeView.remove(timeRecurringMap.recurring);
        currentSituationTypeView.add(timeRecurringMap.oneTime);
    }
}

function formatTimeString(time) {
    var hours = time.getHours();
    var amPM = " AM";
    if (Ti.Platform.is24HourTimeFormat()) {
        if (hours < 10) {
            hours = "0" + hours;
        }
        amPM = "";
    }
    else {
        if (hours > 11) {
            amPM = " PM";
        }
        if (hours > 12) {
            hours -= 12;
        }
        else if (hours == 0) {
            hours = 12;
        }
    }
    var minutes = time.getMinutes();
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    return hours + ":" + minutes + amPM;
}

function buildOneTimeView() {
    var fromLabel = Ti.UI.createLabel({
        text: "From",
        top: 10,
        left: 5,
        font: {
            fontSize: 15
        },
        color: "darkgray"
    });

    var fromDateLabel = Ti.UI.createLabel({
        text: dataModel.time.fromTime.toLocaleDateString(),
        color: CLICKABLE_LINK_COLOR,
        left: 20
    });
    var fromTimeLabelString = formatTimeString(dataModel.time.fromTime);
    var fromTimeLabel = Ti.UI.createLabel({
        text: fromTimeLabelString,
        color: CLICKABLE_LINK_COLOR
    });

    var fromDateTimeDisplayView = Ti.UI.createView({
        width: "auto",
        height: Ti.UI.SIZE,
        layout: "horizontal"
    });
    var leftSplit = Ti.UI.createView({
        width: "70%",
        height: Ti.UI.SIZE
    });
    var rightSplit = Ti.UI.createView({
        width: "30%",
        height: Ti.UI.SIZE
    });
    leftSplit.add(fromDateLabel);
    rightSplit.add(fromTimeLabel);
    fromDateTimeDisplayView.add(leftSplit);
    fromDateTimeDisplayView.add(rightSplit);

    var toLabel = Ti.UI.createLabel({
        text: "To",
        top: 10,
        left: 5,
        font: {
            fontSize: 15
        },
        color: "darkgray"
    });

    var toDateLabel = Ti.UI.createLabel({
        text: dataModel.time.fromTime.toLocaleDateString(),
        color: CLICKABLE_LINK_COLOR,
        left: 20
    });
    var toTimeLabelString = formatTimeString(dataModel.time.toTime);
    var toTimeLabel = Ti.UI.createLabel({
        text: toTimeLabelString,
        color: CLICKABLE_LINK_COLOR
    });

    var toDateTimeDisplayView = Ti.UI.createView({
        width: "auto",
        height: Ti.UI.SIZE,
        layout: "horizontal"
    });
    leftSplit = Ti.UI.createView({
        width: "70%",
        height: Ti.UI.SIZE
    });
    rightSplit = Ti.UI.createView({
        width: "30%",
        height: Ti.UI.SIZE
    });
    leftSplit.add(toDateLabel);
    rightSplit.add(toTimeLabel);
    toDateTimeDisplayView.add(leftSplit);
    toDateTimeDisplayView.add(rightSplit);

    var oneTimeView = Ti.UI.createView({
        width: "auto",
        height: "auto",
        layout: "vertical",
        fromDateLabel: fromDateLabel,
        fromTimeLabel: fromTimeLabel,
        toDateLabel: toDateLabel,
        toTimeLabel: toTimeLabel
    });

    oneTimeView.add(fromLabel);
    oneTimeView.add(fromDateTimeDisplayView);
    oneTimeView.add(toLabel);
    oneTimeView.add(toDateTimeDisplayView);

    fromDateLabel.addEventListener("click", function() {
        openPicker(PICKERS.DATE, "fromTime");
    });
    fromTimeLabel.addEventListener("click", function() {
        openPicker(PICKERS.TIME, "fromTime");
    });
    toDateLabel.addEventListener("click", function() {
        openPicker(PICKERS.DATE, "toTime");
    });
    toTimeLabel.addEventListener("click", function() {
        openPicker(PICKERS.TIME, "toTime");
    });

    return oneTimeView;
}

function buildDatePicker() {
    var picker = Ti.UI.createPicker({
        type:Ti.UI.PICKER_TYPE_DATE,
        minDate:new Date(),
        maxDate:new Date(2030,11,31),
        value:new Date(),
        top:10
    });

    var view = Ti.UI.createView({
        width: "90%",
        height: Ti.UI.SIZE,
        center: 0,
        backgroundColor: "gray",
        borderColor: "darkgray",
        borderRadius: 8,
        borderWidth: 2,
        zIndex: 100,
        layout: "vertical",
        picker: picker,
        modelLabel: ""
    });

    if (OS_IOS) {
        view.backgroundColor = "white";
    }

    var choose = Ti.UI.createButton({
        title: "Choose",
        color: "lightgray",
        height: Ti.UI.SIZE,
        width: "70%",
        bottom: 10
    });

    var cancel = Ti.UI.createButton({
        title: "Cancel",
        color: "gray",
        backgroundColor: "lightgray",
        height: Ti.UI.SIZE,
        width: "70%",
        bottom: 30
    });

    view.add(picker);
    view.add(choose);
    view.add(cancel);
    choose.addEventListener("click", function() {
        applyPick(PICKERS.DATE);
    });
    cancel.addEventListener("click", function() {
        cancelPick(PICKERS.DATE);
    });
    return view;
}


function buildTimePicker() {
    var picker = Ti.UI.createPicker({
        type:Ti.UI.PICKER_TYPE_TIME,
        value:new Date()
    });

    var view = Ti.UI.createView({
        width: "90%",
        height: Ti.UI.SIZE,
        center: 0,
        backgroundColor: "gray",
        borderColor: "darkgray",
        borderRadius: 8,
        borderWidth: 2,
        zIndex: 100,
        layout: "vertical",
        picker: picker,
        modelLabel: ""
    });

    if (OS_IOS) {
        view.backgroundColor = "white";
    }

    var choose = Ti.UI.createButton({
        title: "Choose",
        color: "lightgray",
        height: Ti.UI.SIZE,
        width: "70%",
        bottom: 30
    });

    var cancel = Ti.UI.createButton({
        title: "Cancel",
        color: "gray",
        backgroundColor: "lightgray",
        height: Ti.UI.SIZE,
        width: "70%",
        bottom: 30
    });

    view.add(picker);
    view.add(choose);
    view.add(cancel);
    choose.addEventListener("click", function() {
        applyPick(PICKERS.TIME);
    });
    cancel.addEventListener("click", function() {
        cancelPick(PICKERS.TIME);
    });
    return view;
}

function highlightViews() {
    if (currentSituationTypeView != null) {
        $.mainAddSituationView.remove(currentSituationTypeView);
    }
    currentSituationTypeView = activationTypeMap[selectedSituationType];
    $.mainAddSituationView.add(currentSituationTypeView);
    switch (selectedSituationType) {
        case Alloy.Globals.SituationConstants.Types.TIME:
            setTypeSwitchState(true, false, false);
            updateTimeSubView();
            break;
        case Alloy.Globals.SituationConstants.Types.LOCATION:
            setTypeSwitchState(false, true, false);
            break;
        case Alloy.Globals.SituationConstants.Types.MANUAL:
            setTypeSwitchState(false, false, true);
            break;
    }
}

function setTypeSwitchState(time, location, manual) {
    $.timeSwitch.touchEnabled = !time;
    $.locationSwitch.touchEnabled = !location;
    $.manualSwitch.touchEnabled = !manual;
    if (!time) {
        $.timeSwitch.value = time;
    }
    if (!location) {
        $.locationSwitch.value = location;
    }
    if (!manual) {
        $.manualSwitch.value = manual;
    }
}

if (OS_IOS) {
    var btnSave = Ti.UI.createButton({
        systemButton: Ti.UI.iPhone.SystemButton.SAVE
    });
    btnSave.addEventListener("click", addBook);
    $.situationDetails.rightNavButton = btnSave;
}

initializeDataModel();
initializeControllers();
initializeViewConnections();
populateInitialView();
